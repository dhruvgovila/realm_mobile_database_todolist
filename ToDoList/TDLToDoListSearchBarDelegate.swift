//
//  TDLToDoListSearchBarDelegate.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

extension TDLToDoListViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if(searchText.characters.count > 0)
        {
            listModel.updateItemsWhere(itemName: searchText)
        }
        else
        {
            listModel.updateItems()
        }
        tableViewItemList.reloadData()
    }
}
