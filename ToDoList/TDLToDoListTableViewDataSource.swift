//
//  TDLToDoListTableViewDataSource.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

extension TDLToDoListViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return listModel.items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: TDLToDoListTableViewCell.identifier, for: indexPath) as! TDLToDoListTableViewCell
        let item = listModel.items[indexPath.row]
        cell.updateData(taskName: item["toDoName"] as! String)
        return cell
    }
}
