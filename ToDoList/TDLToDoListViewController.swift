//
//  TDLToDoListViewController.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

class TDLToDoListViewController: UIViewController {

    @IBOutlet weak var buttonSortItemWhenEntered: UIButton!
    @IBOutlet weak var buttonSortItemPriority: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewItemList: UITableView!
    var listModel = TDLToDoListModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.searchBar.text = ""
        self.view.endEditing(true)
        
        buttonSortItemWhenEntered.backgroundColor = UIColor.lightGray
        buttonSortItemPriority.backgroundColor = UIColor.clear
        listModel.updateItemsBySorting(accordingTo: .WhenEntered)
        self.tableViewItemList.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
            
        case TDLToDoListConstant.SegueConstant.ADD:
            break
            
        case TDLToDoListConstant.SegueConstant.EDIT:
            let destinationController = segue.destination as! TDLEditItemViewController
            destinationController.toEditToDoItem = sender as! String
            break
            
        default:
            break
            
        }
    }
    
    @IBAction func actionButtonSortToDoItems(_ sender: UIButton) {
        
        switch sender.tag {
            
        case 1:
            buttonSortItemWhenEntered.backgroundColor = UIColor.lightGray
            buttonSortItemPriority.backgroundColor = UIColor.clear
            listModel.updateItemsBySorting(accordingTo: .WhenEntered)
        case 2:
            buttonSortItemWhenEntered.backgroundColor = UIColor.clear
            buttonSortItemPriority.backgroundColor = UIColor.lightGray
            listModel.updateItemsBySorting(accordingTo: .Priority)
        default:
            break
        }
        
        tableViewItemList.reloadData()
    }

    
}
