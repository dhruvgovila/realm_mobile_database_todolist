//
//  RDBToDoItems.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit
import RealmSwift

class RDBToDoItems: Object {

    dynamic var toDoName: String!
    dynamic var timeStamp: Int64 = 0
    dynamic var priority: Int = 0
    
}
