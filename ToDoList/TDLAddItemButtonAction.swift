//
//  TDLAddItemButtonAction.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit
extension TDLAddItemViewController
{
    @IBAction func actionButtonSaveToDoItem(_ sender: Any) {

        let item = RDBToDoItems()
        item.toDoName = self.textFieldAddItem.text!
        item.timeStamp = Int64(Date().timeIntervalSince1970 * 1000)
        item.priority = priority
        RDBToDoItemsTask.sharedInstance.addData(data: item)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionButtonSetPriority(_ sender: UIButton) {
        
        buttonPriorityHigh.backgroundColor   = .clear
        buttonPriorityMedium.backgroundColor = .clear
        buttonPriorityLow.backgroundColor    = .clear
        
        sender.backgroundColor = UIColor.lightGray
        priority = sender.tag
        
    }
    
}
