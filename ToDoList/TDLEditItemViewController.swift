//
//  TDLEditItemViewController.swift
//  ToDoList
//
//  Created by Rahul Sharma on 29/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

class TDLEditItemViewController: UIViewController {
    
    @IBOutlet weak var textFieldEditToDoItem: UITextField!
    
    var toEditToDoItem: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.textFieldEditToDoItem.text = toEditToDoItem
        self.textFieldEditToDoItem.becomeFirstResponder()
    }
    
}
