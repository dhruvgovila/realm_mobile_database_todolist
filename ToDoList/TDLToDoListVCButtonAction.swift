//
//  TDLToDoListVCButtonAction.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

extension TDLToDoListViewController
{
    @IBAction func actionButtonAddToDoListItem(_ sender: Any) {
        self.performSegue(withIdentifier: TDLToDoListConstant.SegueConstant.ADD, sender: nil)
    }
    
}
