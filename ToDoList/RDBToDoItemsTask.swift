//
//  RDBToDoItemsTask.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

enum Sort {
    case Priority
    case WhenEntered
}

import UIKit
import RealmSwift
class RDBToDoItemsTask: NSObject {
    
    let realm = try! Realm()
    static let sharedInstance = RDBToDoItemsTask()
    
    private override init() {
        
        
        
    }
    
    func removeAllData()
    {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func addData(data itemData:RDBToDoItems)
    {
        try! realm.write {
            realm.add(itemData)
        }
    }
    
    func getAllData() -> Results<RDBToDoItems>
    {
        let toDoItems = realm.objects(RDBToDoItems.self)
        return toDoItems
    }
    
    func getAllDataFor(searchString search:String) -> Results<RDBToDoItems>
    {
        let searchedItems = realm.objects(RDBToDoItems.self).filter("toDoName contains[c] '\(search)'")
        return searchedItems
    }
    
    func getAllDataAfterDeleting(deleteString search:String) -> Results<RDBToDoItems>
    {
        let item = realm.objects(RDBToDoItems.self).filter("toDoName = '\(search)'")
        var toDoItems: Results<RDBToDoItems>!
        try! realm.write {
            realm.delete(item)
            toDoItems = self.getAllData()
        }
        return toDoItems
    }
    
    func getAllDataAfterUpdating(searchString search:String, updateString update:String)
    {
        let item: RDBToDoItems = realm.objects(RDBToDoItems.self).filter("toDoName = '\(search)'").first!
        try! realm.write {
            item.setValue(update, forKeyPath: "toDoName")
        }
    }
    
    func getAllDataAfterSorting(according sort: Sort) -> Results<RDBToDoItems>
    {
        let items: Results<RDBToDoItems>!
        
        switch sort {
        case .Priority:
            items = realm.objects(RDBToDoItems.self).sorted(byKeyPath: "priority")
        case .WhenEntered:
            items = realm.objects(RDBToDoItems.self).sorted(byKeyPath: "timeStamp")
        }
        
        return items
    }
}
