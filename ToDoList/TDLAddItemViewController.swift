//
//  TDLAddItemViewController.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

class TDLAddItemViewController: UIViewController {

    @IBOutlet weak var buttonPriorityHigh: UIButton!
    @IBOutlet weak var buttonPriorityMedium: UIButton!
    @IBOutlet weak var buttonPriorityLow: UIButton!
    @IBOutlet weak var textFieldAddItem: UITextField!
    var priority: Int!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    
}
