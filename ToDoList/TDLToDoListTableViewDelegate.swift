//
//  TDLToDoListTableViewDelegate.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

extension TDLToDoListViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let itemName = getToDoItemName(cellIndex: indexPath)
        self.performSegue(withIdentifier: TDLToDoListConstant.SegueConstant.EDIT, sender: itemName)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete)
        {
            let itemName = getToDoItemName(cellIndex: indexPath)
            listModel.deleteItemsWhere(itemName: itemName!)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func getToDoItemName(cellIndex indexPath: IndexPath) -> String!
    {
        let cell = tableViewItemList.cellForRow(at: indexPath) as! TDLToDoListTableViewCell
        let itemName = cell.labelToDoTaskName.text
        return itemName
    }
}
