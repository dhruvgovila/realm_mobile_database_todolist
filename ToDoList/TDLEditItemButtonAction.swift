//
//  TDLEditItemButtonAction.swift
//  ToDoList
//
//  Created by Rahul Sharma on 29/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

extension TDLEditItemViewController {

    @IBAction func actionButtonEditToDoItem(_ sender: UIBarButtonItem) {
        
        RDBToDoItemsTask.sharedInstance.getAllDataAfterUpdating(searchString: toEditToDoItem, updateString: textFieldEditToDoItem.text!)
        self.navigationController?.popViewController(animated: true)

    }
    
}
