//
//  TDLToDoListTableViewCell.swift
//  ToDoList
//
//  Created by Rahul Sharma on 28/09/17.
//  Copyright © 2017 Dhruv Govila. All rights reserved.
//

import UIKit

class TDLToDoListTableViewCell: UITableViewCell {
    static let identifier = "TDLToDoListTableViewCell"
    
    @IBOutlet weak var labelToDoTaskName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateData(taskName name: String)
    {
        self.labelToDoTaskName.text = name
    }
}
